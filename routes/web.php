<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return 'Olá seja bem-vimdo ao curso!';
});
*/

Route::get('/', 'MainController@main')->name('site.index')->middleware('log.acess');

Route::get('/about','AboutController@about')->name('site.about');

Route::get('/contact','ContactController@contact')->name('site.contact');

Route::post('/contact','ContactController@save')->name('site.contact');

Route::get('/login{error?}', 'LoginController@index')->name('site.login');

Route::post('/login', 'LoginController@authentication')->name('site.login');


Route::middleware('authentication:default')->prefix('/app')->group(function(){

    Route::get('/home', 'HomeController@index')->name('app.home');

    Route::get('/logout', 'LoginController@logout')->name('app.logout');

    Route::get('/source', 'SourceController@index')->name('app.source');

    Route::post('/source/list', 'SourceController@list')->name('app.source.list');

    Route::get('/source/list', 'SourceController@list')->name('app.source.list');

    Route::get('/source/add', 'SourceController@add')->name('app.source.add');

    Route::post('/source/add', 'SourceController@add')->name('app.source.add');

    Route::get('/source/edit/{id}/{msg?}', 'SourceController@edit')->name('app.source.edit');

    Route::get('/source/delete/{id}', 'SourceController@delete')->name('app.source.delete');

    //Products
    Route::resource('/product', 'ProductController');
    Route::delete('product.destroy/{product}', 'ProductController@destroy')->name('product.destroy');

      //ProductsDetails
      Route::resource('/product-detail', 'ProductDetailController');

      Route::resource('client', 'ClientController');
      Route::delete('client.destroy/{client}', 'ClientController@destroy')->name('client.destroy');

      Route::resource('order', 'OrderController');
      //Route::resource('order-product', 'OrderProductController');
      Route::get('order-product/create/{order}', 'OrderProductController@create')->name('order-product.create');
      Route::post('order-product/store/{order}', 'OrderProductController@store')->name('order-product.store');
     // Route::delete('order-product.destroy/{order}/{product}', 'OrderProductController@destroy')->name('order-product.destroy');
     Route::delete('order-product.destroy/{orderProduct}/{order_id}', 'OrderProductController@destroy')->name('order-product.destroy');
});

Route::get('/test/{p1}/{p2}', 'TestController@test')->name('test');

Route::fallback(function(){
    echo "This route doesn't exist, <a href='/'>click here</a> and go to homepage!";
});

//name, category, subject, message
/*
Route::get('/contact/{name}/{category_id}', function(
    string $name = 'Desconhecido',
    int $category_id = 1)//1 - 'Informação'
    {
        echo "Estamos aqui: $name - $category_id";
     })->where('categoty_id', '[0-9]+')->where('name', '[A-Za-z]+');
*/



/* http verbs: get, post, put, patch, delete, options*/
