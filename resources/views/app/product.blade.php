@extends('app.layouts.header')
@section('title', $title)
@section('content')


 <div class="footer">
            <div class="network">
                <h2>Social Network</h2>
                <img src="/img/facebook.png">
                <img src="/img/linkedin.png">
                <img src="/img/youtube.png">
            </div>
            <div class="area-contact">
                <h2>Contact</h2>
                <span>(11) 3333-4444</span>
                <br>
                <span>supergestao@dominio.com.br</span>
            </div>
            <div class="localization">
                <h2>Localization</h2>
                <img src="/img/mapa.png">
            </div>
        </div>
   @endsection
