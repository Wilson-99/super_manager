<div class="top">

            <div class="logo">
                <img src="/img/logo.png">
            </div>

            <div class="menu">
                <ul>
                    <li><a href="{{ route('app.home') }}">Home</a></li>
                    <li><a href="{{ route('client.index') }}">Client</a></li>
                    <li><a href="{{ route('order.index') }}">Order</a></li>
                    <li><a href="{{ route('product.index') }}">Product</a></li>
                     <li><a href="{{ route('app.source') }}">Source</a></li>
                      <li><a href="{{ route('app.logout') }}">Logout</a></li>
                </ul>
            </div>
        </div>
