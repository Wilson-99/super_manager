        @extends('app.layouts.header')
        @section('title', 'list')
        @section('content')

        <div class="content">

            <div class="page-title-2">
                <h1>Sources - List</h1>
            </div>
                <div class="menu">
                    <ul>
                         <li><a href="{{ route('app.source.add') }}">New</a><li>
                       <li><a href="{{ route('app.source') }}">Search</a><li>
                    </ul>
                </div>
                <div class="info-page">
                    <div style="width: 80%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                    <table border="1" width="100%">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Site</th>
                            <th>UF</th>
                            <th>Email</th>
                            <th colspan="2">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($source as $source)
                            <tr>
                                <td>{{ $source->name }}</td>
                                <td>{{ $source->site }}</td>
                                <td>{{ $source->uf }}</td>
                                <td>{{ $source->email }}</td>
                                <td><a href="{{ route ('app.source.delete', $source->id)}}">Delete</a></td>
                                <td><a href="{{ route ('app.source.edit', $source->id)}}">Edit</a></td>
                            </tr>
                            <tr>
                            <td colspan="6">
                            <p>Poducts List</p>
                            <table border="1" style="margin:20px;">
                            <thead>
                            <tr>
                            <th>ID</th>
                            <th>Name:</th>
                            </tr>
                            </thead>
                            <tbody>
                             @foreach ($source->products as $key => $product)
                            <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}}</td>
                            </tr>
                             @endforeach
                            </tbody>
                            </table>
                            </td>
                            </tr>
                      @endforeach
                      </tbody>
                      </table>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
