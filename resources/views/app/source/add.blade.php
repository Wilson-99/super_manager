        @extends('app.layouts.header')
        @section('title', 'add')
        @section('content')

        <div class="content">

            <div class="page-title-2">
                <h1>Sources - Add</h1>
            </div>
                <div class="menu">
                    <ul>
                         <li><a href="{{ route('app.source.add') }}">New</a><li>
                        <li><a href="{{ route('app.source') }}">Back</a><li>
                    </ul>
                </div>
                <div class="info-page">
                {{ $msg ?? '' }}
                    <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                        <form method="POST" action="{{ route('app.source.add') }}">
                        <input type="hidden" name="id" value="{{ $source->id ?? '' }}">
                        @csrf
                        <input type="text" name="name" value="{{ $source->name ?? old('name') }}" placeholder="name" class="black-border">
                         {{ $errors->has('name') ? $errors->first('name') : '' }}
                        <input type="text" name="site" value="{{ $source->site ?? old('site') }}" placeholder="site" class="black-border">
                           {{ $errors->has('site') ? $errors->first('site') : '' }}
                        <input type="text" name="uf" value="{{ $source->uf ?? old('uf') }}" placeholder="uf" class="black-border">
                           {{ $errors->has('uf') ? $errors->first('uf') : '' }}
                        <input type="email" name="email" value="{{ $source->email ?? old('email') }}" placeholder="email" class="black-border">
                        {{ $errors->has('email') ? $errors->first('email') : '' }}
                        <button type="submit" class="black-border">Submit</button>
                        </form>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
