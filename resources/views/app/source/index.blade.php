        @extends('app.layouts.header')
        @section('title', $title)
        @section('content')

        <div class="content">

            <div class="page-title-2">
                <h1>Sources</h1>
            </div>
                <div class="menu">
                    <ul>
                        <li><a href="{{ route('app.source.add') }}">New</a><li>
                      <li><a href="{{ route('app.source') }}">Search</a><li>
                    </ul>
                </div>
                <div class="info-page">
                    <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                        <form method="POST" action="{{ route('app.source.list') }}">
                          @csrf
                        <input type="text" name="name" placeholder="name" class="black-border">
                        <input type="text" name="site" placeholder="site" class="black-border">
                        <input type="text" name="uf" placeholder="uf" class="black-border">
                        <input type="email" name="email" placeholder="email" class="black-border">
                        <button type="submit" class="black-border">Search</button>
                        </form>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
