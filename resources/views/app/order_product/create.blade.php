        @extends('app.layouts.header')

            @section('title', $title)

        @section('content')

        <div class="content">

            <div class="page-title-2">

            <h1>Add Product to Order</h1>

            </div>
                <div class="menu">
                    <ul>
                        <li><a href="{{route('order.index')}}">Back</a></li>
                        <li>Search</li>
                    </ul>
                </div>
                <div class="info-page">
                <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                 <h2>Details of Produts</h2>
                     <span>ID of Product: {{ $order->id }}</span><br>
                     <span>Client: {{ $order->client_id }}</span>
                    <h4>Products Item</h4>
                    <table border="1" width="100%">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Product's Name</th>
                    <th>Date of Order</th>
                    <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ( $order->products as $product)
                     <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->pivot->created_at->format('d/m/Y') }}</td>
                    <td><form id="form_{{ $product->pivot->id }}" method="post" action="{{ route('order-product.destroy', ['orderProduct'=>$product->pivot->id, 'order_id'=>$order->id])}}">
                    @method('DELETE')
                    @csrf
                    <a href="#" onclick="document.getElementById('form_{{ $product->pivot->id }}').submit()">Delete</a></td>
                    </form>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                      <form method="POST" action="{{ route ('order-product.store', ['order'=>$order]) }}">
                         @csrf
                              <select name="product_id">
                            <option>»»Select a Product««</option>
                            @foreach ($products as $product)
                                  <option value="{{$product->id}}" {{ (old('product_id')) == $product->id ? 'selected' : '' }}>{{$product->name}}</option>
                            @endforeach
                        </select>
                         {{ $errors->has('product_id') ? $errors->first('product_id') : '' }}
                         <input type="number" name="qty" placeholder="Quantity" value="{{old('qty') ? old('qty') : '' }}" class="black-border">
                          {{ $errors->has('qty') ? $errors->first('qty') : '' }}

                        <button type="submit" class="black-border">Submit</button>
                        </form>
                    </div>
                </div>
        </div>

             <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
