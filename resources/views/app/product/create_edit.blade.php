        @extends('app.layouts.header')
        @if(isset($products->id))
             @section('title', $title)
            @else
            @section('title', $title)
            @endif

        @section('content')

        <div class="content">

            <div class="page-title-2">
            @if(isset($products->id))
            <h1>Products - Edit</h1>
            @else
            <h1>Products - Add</h1>
            @endif
            </div>
                <div class="menu">
                    <ul>
                        <li><a href="{{route('product.index')}}">Back</a><li>
                    </ul>
                </div>
                <div class="info-page">

                    <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                     @if(isset($products->id))
                <form method="POST" action="{{ route( 'product.update', ['product'=>$products->id] )}}">
                         @csrf
                         @method('PUT')
            @else
                        <form method="POST" action="{{ route ('product.store') }}">
                         @csrf
                         @endif
                              <select name="source_id">
                            <option>»»Select Source««</option>
                            @foreach ($source as $source)
                                  <option value="{{$source->id}}" {{ ($products->source_id ?? old('source_id')) == $source->id ? 'selected' : '' }}>{{$source->name}}</option>
                            @endforeach
                        </select>
                         {{ $errors->has('source_id') ? $errors->first('source_id') : '' }}
                        <input type="hidden" name="id" value="">
                           <label>Name</label>
                        <input type="text" name="name" value="{{ $products->name ?? old('name') }}" placeholder="name" class="black-border">
                         {{ $errors->has('name') ? $errors->first('name') : '' }}
                            <label>Description</label>
                        <input type="text" name="description" value="{{ $products->description ?? old('description') }}" placeholder="description" class="black-border">
                         {{ $errors->has('description') ? $errors->first('description') : '' }}
                            <label>Weight</label>
                        <input type="text" name="weight" value="{{ $products->weight ?? old('weight') }}" placeholder="weight" class="black-border">
                         {{ $errors->has('weight') ? $errors->first('weight') : '' }}
                        <select name="unity_id">
                            <option>»»Select Unity««</option>
                            @foreach ($unities as $unity)
                                  <option value="{{$unity->id}}" {{ ($products->unity_id ?? old('unity_id')) == $unity->id ? 'selected' : '' }}>{{$unity->description}}</option>
                            @endforeach
                        </select>
                         {{ $errors->has('unity_id') ? $errors->first('unity_id') : '' }}
                        <button type="submit" class="black-border">Submit</button>
                        </form>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
