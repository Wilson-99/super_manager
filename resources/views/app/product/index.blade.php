        @extends('app.layouts.header')
        @section('title', $title)
        @section('content')

        <div class="content">

            <div class="page-title-2">
                <h1>Products - List</h1>
            </div>
                <div class="menu">
                    <ul>
                         <li><a href="{{route('product.create')}}">New</a><li>
                       <li><a href="{{route('product-detail.index')}}">Product Details</a><li>
                    </ul>
                </div>
                <div class="info-page">
                    <div style="width: 80%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                    <table border="1" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                             <th>Source</th>
                              <th>Source Site</th>
                            <th>Weight</th>
                            <th>Unity_id</th>
                            <th>Length</th>
                            <th>Width</th>
                             <th>Height</th>
                            <th colspan="3">#</th>
                        </tr>
                    </thead>
                    <tbody>

                      @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->source->name }}</td>
                                <td>{{ $product->source->email }}</td>
                                <td>{{ $product->weight }}</td>
                                <td>{{ $product->unity_id }}</td>
                                 <td>{{ $product->itemDetail->length ?? '' }}</td>
                                  <td>{{ $product->itemDetail->width ?? '' }}</td>
                                   <td>{{ $product->itemDetail->height ?? '' }}</td>
                                <td><a href="{{ route('product.show', ['product'=>$product->id]) }}">Show</a></td>
                                <td><a href="{{ route('product.edit', ['product'=>$product->id]) }}">Edit</a></td>
                            </tr>

                            <tr>
                            <td colspan="12" align="center"><b>Orders</b><br>
                            @foreach ( $product->orders as $order)
                            <a href="{{route('order-product.create', ['order'=>$order->id])}}">
                            Order: {{$order->id}}, &nbsp;
                            </a>
                            @endforeach
                            </td>
                            </tr>
                      @endforeach
                      </tbody>
                      </table>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
