        @extends('app.layouts.header')
        @section('title', 'edit')
        @section('content')

        <div class="content">

            <div class="page-title-2">
                <h1>Products - Edit</h1>
            </div>
                <div class="menu">
                    <ul>
                        <li><a href="{{route('product.index')}}">Back</a><li>
                        <li>Search<li>
                    </ul>
                </div>
                <div class="info-page">

                    <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                        <form method="POST" action="{{ route( 'product.update', ['product'=>$products->id] )}}">
                        <input type="hidden" name="id" value="">
                        @csrf
                        @method('PUT')
                        <input type="text" name="name" value="{{ $products->name ?? old('name') }}" placeholder="name" class="black-border">
                         {{ $errors->has('name') ? $errors->first('name') : '' }}
                        <input type="text" name="description" value="{{ $products->description ?? old('description') }}" placeholder="description" class="black-border">
                         {{ $errors->has('description') ? $errors->first('description') : '' }}
                        <input type="text" name="weight" value="{{ $products->weight ?? old('weight') }}" placeholder="weight" class="black-border">
                         {{ $errors->has('weight') ? $errors->first('weight') : '' }}
                        <select name="unity_id">
                            <option>»»Select Unity««</option>
                            @foreach ($unities as $unity)
                                 <option value="{{$unity->id}}" {{ $products->unity_id ?? old('unity_id') == $unity->id ? 'selected' : '' }}>{{$unity->description}}</option>
                            @endforeach
                        </select>
                         {{ $errors->has('unity_id') ? $errors->first('unity_id') : '' }}
                        <button type="submit" class="black-border">Submit</button>
                        </form>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
