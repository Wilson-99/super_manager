        @extends('app.layouts.header')
        @section('title', $title)
        @section('content')

        <div class="content">

            <div class="page-title-2">
                <h1>Products - Details</h1>
            </div>
                <div class="menu">
                    <ul>
                        <li><a href="{{route('product-detail.index')}}">Back</a><li>
                        <li>Search<li>
                    </ul>
                </div>
                <div class="info-page">

            <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
             <h4>Product</h4>
                <div>Name: {{ $product_detail->item->name }}</div>
                <br>
                <div>Description: {{ $product_detail->item->description }}</div>
                <br>
                    <table border="1" width="100%">
                            <tr>
                                <td>ID Product</td>
                                <td>{{$product_detail->product_id}}</td>
                            </tr>
                              <tr>
                                <td>lenght</td>
                                <td>{{$product_detail->length}}</td>
                            </tr>
                            <tr>
                                <td>width</td>
                                <td>{{$product_detail->width}}</td>
                            </tr>
                            <tr>
                                <td>height</td>
                                <td>{{$product_detail->height}} Kg</td>
                            </tr>
                            <tr>
                                <td>Unity</td>
                                <td>{{$product_detail->unity_id}}</td>
                            </tr>
                      </table>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
