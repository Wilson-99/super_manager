        @extends('app.layouts.header')
        @if(isset($products_details->id))
             @section('title', $title)
            @else
            @section('title', $title)
            @endif

        @section('content')

        <div class="content">

            <div class="page-title-2">
            @if(isset($products_details->id))
            <h1>Products Details - Edit</h1>
            @else
            <h1>Products Details - Add</h1>
            @endif
            </div>
                <div class="menu">
                    <ul>
                        <li><a href="{{route('product-detail.index')}}">Back</a><li>
                        <li>Search<li>
                    </ul>
                </div>
                <div class="info-page">
                <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                    @if(isset($products_details->id))
                <form method="POST" action="{{ route( 'product-detail.update', ['product_detail'=>$products_details->id] )}}">
                         @csrf
                         @method('PUT')


                <div>Name: {{ $products_details->item->name }}</div>
                <br>
                <div>Description: {{ $products_details->item->description }}</div>
                <br>
                        @else
                        <form method="POST" action="{{ route ('product-detail.store') }}">
                         @csrf
                         @endif
                        <input type="hidden" name="id" value="">
                           <label>Product ID</label>
                        <input type="text" name="product_id" value="{{ $products_details->product_id ?? old('product_id') }}" placeholder="Product ID" class="black-border">
                         {{ $errors->has('product_id') ? $errors->first('product_id') : '' }}
                            <label>Length</label>
                        <input type="text" name="length" value="{{ $products_details->length ?? old('length') }}" placeholder="Length" class="black-border">
                         {{ $errors->has('length') ? $errors->first('length') : '' }}
                            <label>Width</label>
                        <input type="text" name="width" value="{{ $products_details->width ?? old('width') }}" placeholder="Width" class="black-border">
                         {{ $errors->has('width') ? $errors->first('width') : '' }}
                            <label>Height</label>
                        <input type="text" name="height" value="{{ $products_details->height ?? old('height') }}" placeholder="Height" class="black-border">
                         {{ $errors->has('height') ? $errors->first('height') : '' }}
                        <select name="unity_id">
                            <option>»»Select Unity««</option>
                            @foreach ($unities as $unity)
                                  <option value="{{$unity->id}}" {{ ($products_details->unity_id ?? old('unity_id')) == $unity->id ? 'selected' : '' }}>{{$unity->description}}</option>
                            @endforeach
                        </select>
                         {{ $errors->has('unity_id') ? $errors->first('unity_id') : '' }}
                        <button type="submit" class="black-border">Submit</button>
                        </form>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
