        @extends('app.layouts.header')
        @section('title', $title)
        @section('content')

        <div class="content">
            <div class="page-title-2">
                <h1>Products Details - List</h1>
            </div>
                <div class="menu">
                    <ul>
                         <li><a href="{{route('product-detail.create')}}">New</a><li>
                       <li>Search<li>
                    </ul>
                </div>
                <div class="info-page">
                    <div style="width: 80%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                    <table border="1" width="100%">
                    <thead>
                        <tr>
                            <th>Lenght</th>
                            <th>Width</th>
                            <th>Height</th>
                            <th>Product_id</th>
                            <th>Unity_id</th>
                            <th>Show</th>
                            <th colspan="2">#</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($products_details as $product_detail)
                            <tr>
                                <td>{{ $product_detail->length }}</td>
                                <td>{{ $product_detail->width }}</td>
                                <td>{{ $product_detail->height }}</td>
                                <td>{{ $product_detail->product_id }}</td>
                                <td>{{ $product_detail->unity_id }}</td>
                                <td><a href="{{ route('product-detail.show', ['product_detail'=>$product_detail->id]) }}">Show</a></td>
                               <td><form id="form_{{$product_detail->id}}" method="POST" action="{{ route('product-detail.destroy', ['product_detail'=>$product_detail->id]) }}">
                                @method('DELETE')
                                @csrf
                                <!--<button type="submit">Delete</button>-->
                                <a href="#" onclick="document.getElementById('form_{{$product_detail->id}}').submit()">Delete</a></form></td>
                                <td><a href="{{ route('product-detail.edit', ['product_detail'=>$product_detail->id]) }}">Edit</a></td>
                            </tr>
                      @endforeach
                      </tbody>
                      </table>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
