        @extends('app.layouts.header')
        @section('title', $title)
        @section('content')

        <div class="content">

            <div class="page-title-2">
                <h1>Order - List</h1>
            </div>
                <div class="menu">
                    <ul>
                         <li><a href="{{route('order.create')}}">New</a><li>
                    </ul>
                </div>
                <div class="info-page">
                    <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                    <table border="1" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Client</th>
                            <th colspan="3">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                      @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->client_id }}</td>
                                <td><a href="{{ route('order-product.create', ['order'=>$order->id]) }}">Add product</a></td>
                            </tr>
                      @endforeach
                      </tbody>
                      </table>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
