        @extends('app.layouts.header')

            @section('title', $title)


        @section('content')

        <div class="content">

            <div class="page-title-2">

            <h1>Order - Add</h1>

            </div>
                <div class="menu">
                    <ul>
                        <li><a href="{{route('order.index')}}">Back</a><li>
                    </ul>
                </div>
                <div class="info-page">

                    <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">

                        <form method="POST" action="{{ route ('order.store') }}">
                         @csrf
                              <select name="client_id">
                            <option>»»Select Client««</option>
                            @foreach ($clients as $client)
                                  <option value="{{$client->id}}" {{ ($order->client_id ?? old('client_id')) == $client->id ? 'selected' : '' }}>{{$client->name}}</option>
                            @endforeach
                        </select>
                         {{ $errors->has('client_id') ? $errors->first('client_id') : '' }}
                        <input type="hidden" name="id" value="">

                        <button type="submit" class="black-border">Submit</button>
                        </form>
                    </div>
                </div>
        </div>


            <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
