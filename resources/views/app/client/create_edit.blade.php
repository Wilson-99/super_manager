        @extends('app.layouts.header')
        @if(isset($client->id))
             @section('title', $title)
            @else
            @section('title', $title)
            @endif

        @section('content')

        <div class="content">

            <div class="page-title-2">
            @if(isset($client->id))
            <h1>Client - Edit</h1>
            @else
            <h1>Client - Add</h1>
            @endif
            </div>
                <div class="menu">
                    <ul>
                        <li><a href="{{route('client.index')}}">Back</a><li>
                    </ul>
                </div>
                <div class="info-page">

                    <div style="width: 30%; margin-left: auto; margin-right: auto; margin-top: 5rem;">
                     @if(isset($client->id))
                <form method="POST" action="{{ route( 'client.update', ['client'=>$client->id] )}}">
                         @csrf
                         @method('PUT')
                    @else
                        <form method="POST" action="{{ route ('client.store') }}">
                         @csrf
                           @endif
                        <input type="hidden" name="id" value="">
                        <input type="text" name="name" value="{{ $client->name ?? old('name') }}" placeholder="name" class="black-border">
                         {{ $errors->has('name') ? $errors->first('name') : '' }}
                        <button type="submit" class="black-border">Submit</button>
                        </form>
                    </div>
                </div>
        </div>

             <div class="footer">
                    <div class="network">
                        <h2>Social Network</h2>
                        <img src="/img/facebook.png">
                        <img src="/img/linkedin.png">
                        <img src="/img/youtube.png">
                    </div>
                    <div class="area-contact">
                        <h2>Contact</h2>
                        <span>(11) 3333-4444</span>
                        <br>
                        <span>supergestao@dominio.com.br</span>
                    </div>
                    <div class="localization">
                        <h2>Localization</h2>
                        <img src="/img/mapa.png">
                    </div>
                </div>
        @endsection
