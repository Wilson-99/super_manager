<!DOCTYPE html>
<html lang="en">
    <head>
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/style.css">

    </head>

    <body>

        @include('site.layouts._partial.top')

        @yield('content')

    </body>
</html>
