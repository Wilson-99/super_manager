{{ $slot }}

<form method="POST" action="{{route('site.contact')}}">
                    @csrf
                        <input type="text" value="{{old('name')}}" name="name" placeholder="Name" class="{{$class}}">
                         {{ $errors->has('name') ? $errors->first('name') : '' }}
                        <br>
                        <input type="text" value="{{old('phone')}}" name="phone" placeholder="Phone" class="{{$class}}">
                         {{ $errors->has('phone') ? $errors->first('phone') : '' }}
                        <br>
                        <input type="email" value="{{old('email')}}" name="email" placeholder="E-mail" class="{{$class}}">
                         {{ $errors->has('email') ? $errors->first('email') : '' }}
                        <br>
                        <select name="why" class="{{$class}}">
                            <option value="">What's up?</option>
                            <option value="Doubt">Doubt</option>
                            <option value="Praise">Praise</option>
                            <option value="Complain">Complain</option>
                        </select>
                         {{ $errors->has('why') ? $errors->first('why') : '' }}
                        <br>
                        <textarea name="message" class="{{$class}}" placeholder="Write something"></textarea>
                         {{ $errors->has('message') ? $errors->first('message') : '' }}
                        <br>
                        <button type="submit" class="{{$class}}">Apply</button>
                    </form>

