       @extends('site.layouts.header')
       @section('title', $title)
        @section('content')
        <div class="content">

            <div class="left">
                <div class="info">
                    <h1>Super Manager System</h1>
                    <p>Software ideal for your company.<p>
                    <div class="call">
                        <img src="{{asset('/img/check.png')}}">
                        <span class="white-text">Complet manager and easily to use.</span>
                    </div>
                    <div class="call">
                        <img src="{{asset('/img/check.png')}}">
                        <span class="white-text">Your company in cloud</span>
                    </div>
                </div>

                <div class="video">
                    <img src="{{asset('/img/admin.jpeg')}}">
                </div>
            </div>

            <div class="right">
                <div class="contact">
                    <h1 align="center">CONTACT US</h1>
                    <p>&nbsp;&nbsp;If you have a doubt, fill the form and contact us.<p>
                    <p>&nbsp;&nbsp;We'll check your message and reply you as soon as possible, within 48 hours.</p>
                     @component('site.layouts._components.form_contact', ['class'=>'white-border'])
                    @endcomponent
                </div>
            </div>
        </div>
   @endsection
