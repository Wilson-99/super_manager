@extends('site.layouts.header')
@section('title', $title)
@section('content')

        <div class="page-content">
            <div class="title">
                <h1>Login</h1>
            </div>

            <div class="page-info">
                <div style="width: 30%; margin-left: auto; margin-right: auto;">
                    <form method="POST" action="{{ route('site.login') }}">
                        @csrf
                        <label>Email</label>
                        <input type="email" name="email" placeholder="email" class="black-border" value="{{ old('email') }}">
                        {{ $errors->has('email') ? $errors->first('email') : '' }}
                        <label>Password</label>
                        <input type="password" name="password" placeholder="password" class="black-border" value="{{ old('password') }}">
                        {{ $errors->has('password') ? $errors->first('password') : '' }}
                        <button type="submit" class="black-border">Login</button>
                    </form>
                    {{ isset($error) && $error != '' ? $error : '' }}
                </div>
            </div>
        </div>

        @endsection


