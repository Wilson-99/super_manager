<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $table = 'sources';
    protected $fillable = ['name', 'site', 'uf', 'email'];

    public function products(){
        return $this->hasMany('App\item', 'source_id', 'id');
        //return $this->hasMany('App\item');
    }
}
