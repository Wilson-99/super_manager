<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'products';
    protected $fillable = ['source_id','name', 'description', 'weight', 'unity_id'];

     public function itemDetail(){
         return $this ->hasOne('App\ItemDetail', 'product_id', 'id');

         /*Product has one productDetail
         A register has joined with a product_detail (fk) -> product_id
         product (pk) -> id*/
     }

     public function source(){
        return $this->belongsTo('App\Source');
     }

     public function orders(){
        return $this->belongsToMany('App\Order', 'orders_products','product_id','order_id');
        /*
        3- Represents FK of the table linked model in relationship table
        4- Represents FK of the table linked model is using in relationship table that we implements
        */
     }
}
