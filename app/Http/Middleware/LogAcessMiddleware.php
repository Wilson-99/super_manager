<?php

namespace App\Http\Middleware;

use Closure;

use App\LogAcess;

class LogAcessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$request - manipulate
        //reply - manipulate

        $ip = $request->server->get('REMOTE_ADDR');
        $route = $request->getRequestUri();
        LogAcess::create(['log'=>"IP $ip asked the $route route"]);
        //return $next($request);
        $answer = $next($request);

        $answer ->setStatusCode(210, 'We modified the answer and text!!');
        return $answer;
    }
}
