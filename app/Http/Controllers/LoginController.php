<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;

use App\User;

class LoginController extends Controller
{
    public function index(Request $request){

        $error = '';

        if($request ->get('error') == 1){
            $error = 'User or Password does not exist!';
        }

        if($request ->get('error') == 2){
            $error = 'Do login in order to acess the page!';
        }

        return view('site.login', ['title'=>'Login', 'error'=>$error]);
    }

    public function authentication(Request $request){
        //validate rulls
        $rulls = [
            'email' => 'email',
            'password' => 'required'
        ];

        //feedback message
        $feedback = [
            'email' => 'Required field!',
            'password.required' => 'Required field!'
        ];

        $request ->validate($rulls, $feedback);

        $email = $request->get('email');
        $password = $request->get('password');

        echo $email and $password;

        //begin user model
        $user = new User();

        $exist = $user->where('email', $email)->where('password', $password)->get()->first();

        if(isset($exist->name)) {

            session_start();
            $_SESSION['name'] = $exist->name;
            $_SESSION['email'] = $exist->email;
            return redirect()->route('app.home');
        } else {
            return redirect()->route('site.login',['error'=>1]);
        }
       }

       public function logout(){
        session_destroy();
        return redirect()->route('site.index');
       }
}
