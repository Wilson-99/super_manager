<?php

namespace App\Http\Controllers;

use App\Item;
use App\ProductDetail;
use Illuminate\Http\Request;
use App\Product;
use App\Unity;
use App\ItemDetail;

class ProductDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product_detail = ProductDetail::all();
        return view('app.product_detail.index',['title'=>'Product_Detail', 'products_details' => $product_detail]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unity = Unity::all();
        return view('app.product_detail.create_edit', ['title'=>'Create', 'unities'=> $unity]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rulls = [
            'product_id' => 'exists:products,id',
            'length' => 'required|integer',
            'width' => 'required|integer',
            'height' => 'required|integer',
            'unity_id' => 'exists:unities,id',
        ];

        $feedback =[
            'product_id.exists' => 'It must be a real product',
            'length.integer' => 'It must be an integer number',
            'width.integer' => 'It must be an integer number',
            'height.integer' => 'It must be an integer number',
            'unity_id.exists' => 'It must be a real unity'
        ];

        $request -> validate($rulls, $feedback);
        ProductDetail::create($request->all());

       return redirect()->route('product-detail.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product_detail = ItemDetail::find($id);
        return view('app.product_detail.show', ['title'=>'Show', 'product_detail'=> $product_detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_detail = ItemDetail::find($id);
        $unity = Unity::all();
        return view('app.product_detail.create_edit', ['title'=>'Edit', 'products_details'=> $product_detail, 'unities' => $unity]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductDetail  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductDetail $product_detail)
    {
        $product_detail->update($request->all());
       return redirect()->route('product-detail.show', ['title'=>'Update', 'product_detail' => $product_detail->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductDetail  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductDetail $product_detail)
    {
        $product_detail->delete();
        return redirect()->route('product-detail.index');
    }
}
