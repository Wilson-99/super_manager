<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SiteContact;

class ContactController extends Controller
{
    public function contact(Request $request){
        /*
        echo '<pre>';
        print_r($request->all());
        echo '</pre>';
        echo $request->input('name');
        echo '<br>';
        echo $request->input('email');
       */

       $contact = new SiteContact();
       $contact -> name = $request->input('name');
       $contact -> phone = $request->input('phone');
       $contact -> email = $request->input('email');
       $contact -> why = $request->input('why');
       $contact -> message = $request->input('message');

       //print_r($contact->getAttributes());
       $contact->save();
       return view('site.contact', ['title'=>'Contact']);

       }

       public function save(Request $request){
        $rulls = [
            'name' => 'required|min:5',
            'phone' => 'required',
            'email' => 'required',
            'why' => 'required',
            'message' => 'required',
        ];

        $feedback =[
            'name.min' => 'It must have at least 5 character',
            'phone' => 'It must be an real number',
            'email.email' => 'Inser a real email',
            'why' => 'It must be a real porpos',
            'message' => 'It must be a message'
        ];

        $request -> validate($rulls, $feedback);
        SiteContact::create($request->all());
        return redirect()->route('site.index');
       }


}
