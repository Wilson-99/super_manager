<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $client = Client::all();
        return view('app.client.index',['title'=>'Client', 'clients'=>$client]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.client.create_edit', ['title'=>'Create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rulls = [
            'name' => 'required|min:3|max:40'
        ];

        $feedback = [
            'required' => 'This field is required',
            'name.min' => 'The name must have at least 3 character',
            'name.max' => 'The name must have 40 character'
        ];

        $request->validate($rulls, $feedback);
        $client = new Client();
        $client->name = $request->get('name');
        $client->save();

        return redirect()->route('client.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('app.client.show', ['title'=>'Show', 'clients'=> $client]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $clients = Client::all();
        return view('app.client.create_edit', ['title'=>'Edit', 'client'=> $client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $rulls = [
            'name' => 'required|min:3|max:40'
        ];

        $feedback = [
            'required' => 'This field is required',
            'name.min' => 'The name must have at least 3 character',
            'name.max' => 'The name must have 40 character'
        ];

        $request->validate($rulls, $feedback);
        $client->update($request->all());
        return redirect()->route('client.show', ['title'=>'show', 'client' => $client->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return redirect()->route('client.index');
    }
}
