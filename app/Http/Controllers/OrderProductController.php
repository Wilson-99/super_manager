<?php

namespace App\Http\Controllers;
use App\Order;
use App\OrderProduct;
use App\Product;
use Illuminate\Http\Request;

class OrderProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Order $order)
    {
        $products = Product::all();
        //$order->products;
        return view('app.order_product.create', ['title'=>'Order Produt','order'=>$order, 'products'=>$products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {
        $rulls = [
            'product_id' => 'exists:products,id',
            'qty' => 'required'
        ];

        $feedback =[
            'required' => 'Required field',
            'product_id.exists' => 'It must be a real name'
        ];

        $request -> validate($rulls, $feedback);
        /*
        $orderProduct = new OrderProduct();
        $orderProduct->order_id = $order->id;
        $orderProduct->product_id = $request->get('product_id');
        $orderProduct->qty = $request->get('qty');
        $orderProduct->save();*/

        /*$order->products()->attach($request->get('product_id'),
        ['qty'=>$request->get('qty')]
    );*/

    $order->products()->attach([
        $request->get('product_id')=>['qty'=>$request->get('qty')]
    ]);

        return redirect()->route('order-product.create', ['order'=>$order->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderProduct  $orderproduct
     * @return \Illuminate\Http\Response
     */
    public function show(OrderProduct $orderproduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderProduct  $orderproduct
     * @return \Illuminate\Http\Response
     */
    //public function destroy(Order $order, Product $product)
    public function destroy(OrderProduct $orderProduct, $order_id)
    {
       /*OrderProduct::where([
        'order_id'=>$order->id,
        'product_id'=>$product->id
       ])->delete();*/

       /*$order->products()->detach($product->id);
        print_r($order->getAttributes());
        echo 'hr';
        print_r($product->getAttributes());*/

        $orderProduct->delete();
        return redirect()->route('order-product.create', ['order'=>$order_id]);
    }
}
