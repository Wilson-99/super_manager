<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductDetail;
use App\Unity;
use Illuminate\Http\Request;
use App\Item;
use App\Source;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Item::all();

       /* foreach($products as $key => $product){
            //print_r($product->getAttributes());
            //echo '<br><br>';

           $productDetail = ProductDetail::where('product_id', $product->id)->first();

            if(isset($productDetail)){
                //print_r($productDetail->getAttributes());

                $products[$key]['length'] = $productDetail->length;
                $products[$key]['width'] = $productDetail->width;
                $products[$key]['height'] = $productDetail->height;
            }

           // echo '<hr>';
        }*/

        return view('app.product.index',['title'=>'Product', 'products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unities = Unity::all();
        $source = Source::all();
        return view('app.product.create_edit', ['title'=>'Create', 'unities'=> $unities, 'source'=> $source]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rulls = [
            'source_id' => 'exists:sources,id',
            'name' => 'required|min:3|max:40',
            'description' => 'required|min:5|max:2000',
            'weight' => 'required|integer',
            'unity_id' => 'exists:unities,id',
        ];

        $feedback =[
            'required' => 'Required field',
            'source_id.exists' => 'It must be a real name',
            'name.min' => 'You must put at least 3 character',
            'name.max' => 'The limit is 40 character',
            'description.min' => 'You must put at least 3 character',
            'description.max' => 'The limit is 2 character',
            'weight.integer' => 'It must be an integer number',
            'unity_id.exists' => 'It must be a real unity'
        ];

        $request -> validate($rulls, $feedback);
        Product::create($request->all());
       /*$product = new Product();
       $name = $request->get('name');
       $description = $request->get('description');

       $name = strtoupper($name);

       $product -> name = $name;
       $product -> description = $description;

       $product -> save();*/
       return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('app.product.show', ['title'=>'Show', 'products'=> $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $unity = Unity::all();
        $source = Source::all();
        //return view('app.product.edit', ['products'=> $product, 'unities' => $unity]);
        return view('app.product.create_edit', ['title'=>'Edit', 'products'=> $product, 'unities' => $unity, 'source'=> $source]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $product)
    {
       /*print_r($request->all());//playload
       echo '<br><br>';
       print_r($product->getAttributes());//object instant in last status*/
        //dd($request->all());
        $rulls = [
            'source_id' => 'exists:sources,id',
            'name' => 'required|min:3|max:40',
            'description' => 'required|min:5|max:2000',
            'weight' => 'required|integer',
            'unity_id' => 'exists:unities,id',
        ];

        $feedback =[
            'required' => 'Required field',
            'source_id.exists' => 'It must be a real name',
            'name.min' => 'You must put at least 3 character',
            'name.max' => 'The limit is 40 character',
            'description.min' => 'You must put at least 3 character',
            'description.max' => 'The limit is 2 character',
            'weight.integer' => 'It must be an integer number',
            'unity_id.exists' => 'It must be a real unity'
        ];

        $request -> validate($rulls, $feedback);
       $product->update($request->all());
       return redirect()->route('product.show', ['title'=>'Update', 'product' => $product->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('product.index');
    }
}
