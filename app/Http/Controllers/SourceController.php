<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Source;

class SourceController extends Controller
{
    public function index(){
        return view('app.source.index',['title'=>'Source']);
    }

    public function list(Request $request){

        $source = Source::where('name', 'like', '%'.$request->input('name').'%' )
        ->where('site', 'like', '%'.$request->input('site').'%' )
        ->where('uf', 'like', '%'.$request->input('uf').'%' )
        ->where('email', 'like', '%'.$request->input('email').'%' )->get();

        return view('app.source.list', ['source' => $source]);
    }

    public function add(Request $request){
        $msg = '';

        //insert
        if($request->input('_token') != '' && $request->input('id') == ''){
            //validate
            $rulls = [
                'name' => 'required|min:3|max:40',
                'site' => 'required',
                'uf' => 'required|min:2|max:2',
                'email' => 'email'
            ];

            $feedback= [
                'required' => 'Required field',
                'name.min' => 'You must put at least 3 character',
                'name.max' => 'The limit is 40 character',
                'uf.min' => 'You must put at least 3 character',
                'uf.max' => 'The limit is 2 character',
                'email.email' => 'Fill correctly',
            ];

            $request->validate($rulls, $feedback);
            $source = new Source();
            $source->create($request->all());

            //redirect


            //data view
            $msg = 'Subscribe successfully';
        }

        //edition
        if($request->input('_token') != '' && $request->input('id') != ''){
            $source = Source::find($request->input('id'));
            $update = $source->update($request->all());

            if($update){
                $msg = 'Update successfully!';
            } else{
                $msg = 'Something was wrong!';
            }
            return redirect()->route('app.source.edit', ['id' => $request->input('id'), 'msg'=>$msg]);
        }

        return view('app.source.add', ['msg'=>$msg]);
    }

    public function edit($id, $msg = ''){
        $source = Source::find($id);
       return view('app.source.add', ['source' => $source, 'msg' => $msg]);
    }

    public function delete($id){
        Source::find($id)->delete();
        //Source::find($id)->forceDelete();

        return redirect()->route('app.source.list');
    }
}
