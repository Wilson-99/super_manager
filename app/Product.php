<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'weight', 'unity_id'];

    public function poductDetail(){
        return $this ->hasOne('App\ProductDetail');

       /* Product has one productDetail
        A register has joined with a product_detail (fk) -> product_id
        product (pk) -> id*/
    }
}
