<?php

use Illuminate\Database\Seeder;
use App\SiteContact;
use Illuminate\Support\Facades\DB;

class SiteContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
          //instancing data
          $contact = new SiteContact();
          $contact -> name = 'contact 100';
          $contact -> phone = '(+244) 222 778 456';
          $contact -> email = 'contact100@mail.com';
          $contact -> why = 1;
          $contact -> message = 'I wanna get more info about SM';
          $contact -> save();

          //using create method(pay atention with fillable atribute of class)
          SiteContact::create([
              'name'=> 'contact200',
              'phone' => '(+244) 224 568 121',
              'email' => 'contact200@contact.com',
              'why' => 2,
              'message' => 'It is powerfull and one of the best system, I love this gam!'
          ]);

          //insert
          DB::table('site_contacts')->insert([
              'name'=> 'contact300',
              'phone' => '(+244) 931 452 789',
              'email' => 'contact300@contact.com',
              'why' => 3,
            'message' => 'I can not look after my businesses properly, this system is shit.'
          ]);
        */

    factory(SiteContact::class,100)->create();
    }
}
