<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Source;

class SourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //instancing data
        $source = new Source();
        $source -> name = 'Source 100';
        $source -> site = 'source100.ao.com';
        $source -> uf = 'BA';
        $source -> email = 'source100@mail.com';
        $source -> save();

        //using create method(pay atention with fillable atribute of class)
        Source::create([
            'name'=> 'Source200',
            'site' => 'source200.com.ao',
            'uf' => 'Lo',
            'email' => 'source200@source.com'
        ]);

        //insert
        DB::table('sources')->insert([
            'name'=> 'Source300',
            'site' => 'source300.com.ao',
            'uf' => 'LA',
            'email' => 'source300@source.com'
        ]);
    }
}
