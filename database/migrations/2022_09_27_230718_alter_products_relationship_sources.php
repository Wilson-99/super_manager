<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterProductsRelationshipSources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //It's creating the column in products, which will receive the fk
        Schema::table('products', function(Blueprint $table){
            //Insert a register in source in order to mantein the relationship
            $source_id = DB::table('sources')->insertGetId([
            'name' =>'Default Source SG',
            'site' =>'defaultsource.co.ao',
            'uf' =>'LO',
            'email' =>'sourcesg@contact.com'
            ]);

            $table->unsignedBigInteger('source_id')->default($source_id)->after('id');
            $table->foreign('source_id')->references('id')->on('sources');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table){
        $table->dropForeign('products_source_id_foreign');
        $table->dropColumn('source_id');
        });
    }
}
