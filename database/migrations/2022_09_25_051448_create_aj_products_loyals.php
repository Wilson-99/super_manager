<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAjProductsLoyals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //loyals table
        Schema::create('loyals', function (Blueprint $table) {
            $table->id();
            $table->string('loyal', 40);
            $table->timestamps();
        });

        //products_loyals table
        Schema::create('products_loyals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loyal_id');
            $table->unsignedBigInteger('product_id');
            $table->decimal('sell_price');
            $table->integer('min_stock');
            $table->integer('max_stock');
            $table->timestamps();

            //constraint fk
            $table->foreign('loyal_id')->references('id')->on('loyals');
            $table->foreign('product_id')->references('id')->on('products');
        });

        //erasing column of products_table
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('sell_price', 'min_stock', 'max_stock');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //add columns of products_table
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('sell_price', 8, 2);
            $table->integer('min_stock');
            $table->integer('max_stock');
        });

        Schema::dropIfExists('products_loyals');

        Schema::dropIfExists('loyals');
    }
}
