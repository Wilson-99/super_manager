<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unities', function (Blueprint $table) {
            $table->id();
            $table->string('unity', 5); //cm, mm, kg
            $table->string('description', 50);
            $table->timestamps();
        });

         //we must add the relationship with products_table
         Schema::table('products', function(Blueprint $table){
            $table->unsignedBigInteger('unity_id');
            $table->foreign('unity_id')->references('id')->on('unities');
        });

        //we must add the relationship with product_details
        Schema::table('product_details', function(Blueprint $table){
            $table->unsignedBigInteger('unity_id');
            $table->foreign('unity_id')->references('id')->on('unities');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {

        //delete the relationship with product_details
        Schema::table('product_details', function(Blueprint $table){
            //erase fk
            $table->dropForeign('product_details_unity_id_foreign');
            //erase the column unity_id
            $table->dropColumn('unity_id');
        });

        //delete the relationship with products
        Schema::table('products', function(Blueprint $table){
            //erase fk
            $table->dropForeign('products_unity_id_foreign');
            //erase the column unity_id
            $table->dropColumn('unity_id');
        });

        //delete table
        Schema::dropIfExists('unities');
    }
}
